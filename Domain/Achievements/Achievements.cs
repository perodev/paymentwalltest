﻿namespace Domain.Achievements
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    partial class AchievementsDataContext
    {
    }

    //LANGUAGE
    partial class Language
    {
        public static Language Get(string languageCode)
        {
            Language lang = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                IQueryable<Language> languages = dbContext.Languages.Where(r => r.languageCode == languageCode);
                if (languages.Any())
                {
                    lang = languages.ToList().First();
                }
            }
            return lang;
        }
    }

    //ACHIEVEMENT
    partial class Achievement
    {
        public static List<Achievement> GetList(int platformId, bool includeInactive = false)
        {
            List<Achievement> achList = new List<Achievement>();

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                IQueryable<Achievement> achievements = dbContext.Achievements.Where(r => r.platformId == platformId).OrderBy(r => r.orderId);
                if (includeInactive == false)
                {
                    achievements = achievements.Where(r => r.isActive == true).OrderBy(r => r.orderId);
                }
                achList = achievements.ToList();
            }

            return achList;
        }

        public static Achievement GetByID(int achievementId)
        {
            Achievement achievement = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                IQueryable<Achievement> achievements = dbContext.Achievements.Where(r => r.achievementId == achievementId);
                if (achievements.Any())
                {
                    achievement = achievements.ToList().First();
                }
            }
            return achievement;
        }

        public static Achievement findPreviousChallenge(int platformId, int achievementId)
        {
            Achievement ach = Achievement.GetByID(achievementId);
            Achievement achPrevious = null;
            int orderId = ach.orderId;

            if (ach.isChallenge == false) { return null; }
            if (ach.orderId <= 0) { return null; }

            List<Achievement> list = Achievement.GetList(platformId);
            while (--orderId > 0)
            {
                achPrevious = list.Find(a => a.orderId == orderId && a.isChallenge == true);
                if (achPrevious != null)
                {
                    break;
                }
            }
            return achPrevious;
        }
    }

    //ACHIEVEMENT TEXT
    partial class AchievementText
    {
        public static AchievementText Get(int achievementId, string languageCode)
        {
            AchievementText text = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                Language lang = Language.Get(languageCode);
                IQueryable<AchievementText> texts = dbContext.AchievementTexts.Where(r => r.achievementId == achievementId && r.languageId == lang.languageId);
                if (texts.Any())
                {
                    text = texts.ToList().First();
                }
            }
            return text;
        }

        public static AchievementTextResult GetResult(int achievementId, string languageCode)
        {
            AchievementTextResult result = new AchievementTextResult() { name = "", completed = "", reward = "" };
            AchievementText text = Get(achievementId, languageCode);
            if (text != null)
            {
                result.name = text.name;
                result.completed = text.completed;
                result.reward = text.reward;
            }
            return result;
        }
    }

    //ACHIEVEMENT RULE
    partial class AchievementRule
    {
        public static List<AchievementRule> GetList(int achievementId)
        {
            List<AchievementRule> list = new List<AchievementRule>();

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                list = (
                    from ar in dbContext.AchievementRules
                    where ar.achievementId == achievementId
                    select ar
                ).ToList();
            }

            return list;
        }

        public static AchievementRule GetByID(int ruleId)
        {
            AchievementRule rule = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                IQueryable<AchievementRule> rules = dbContext.AchievementRules.Where(r => r.ruleId == ruleId);
                if (rules.Any())
                {
                    rule = rules.ToList().First();
                }
            }

            return rule;
        }

        public static List<RuleResult> GetResultList(int platformId, int profileId, int achievementId, string languageCode)
        {
            List<RuleResult> list = new List<RuleResult>();
            List<AchievementActionLog> logList = AchievementActionLog.GetList(platformId, profileId);

            //if challenge has previous challenge only take actions that were logged after previous challenge is accomplished
            Achievement previousChallenge = Achievement.findPreviousChallenge(platformId, achievementId);
            if (previousChallenge != null)
            {
                ProfileAchievement previousProfileChallenge = ProfileAchievement.GetByAchievementID(platformId, profileId, previousChallenge.achievementId);
                if (previousProfileChallenge != null)
                {
                    logList = logList.Where(r => r.createdOn.CompareTo(previousProfileChallenge.confirmedOn) > 0).ToList();
                }
            }

            List<AchievementRule> rules = GetList(achievementId);
            foreach (AchievementRule rule in rules)
            {
                AchievementAction action = AchievementAction.GetByID(rule.actionId);
                AchievementActionText actionText = AchievementActionText.Get(action.actionId, languageCode);
                AchievementRuleText ruleText = AchievementRuleText.Get(rule.ruleId, languageCode);

                RuleResult rr = new RuleResult()
                {
                    ruleAction = new ActionResult() { actionCode = action.actionCode, name = action.name, description = actionText.name, points = action.points },
                    targetCount = rule.amount,
                    currentCount = AchievementActionLog.GetActionCount(platformId, profileId, logList, rule),
                    rulePoints = rule.points,
                    specificValue = rule.specificValue,
                    description = ruleText.description
                };
                list.Add(rr);
            }

            return list;
        }
    }

    //ACHIEVEMENT RULE TEXT
    partial class AchievementRuleText
    {
        public static AchievementRuleText Get(int ruleId, string languageCode)
        {
            AchievementRuleText text = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                Language lang = Language.Get(languageCode);
                IQueryable<AchievementRuleText> texts = dbContext.AchievementRuleTexts.Where(r => r.ruleId == ruleId && r.languageId == lang.languageId);
                if (texts.Any())
                {
                    text = texts.ToList().First();
                }
            }
            return text;
        }
    }

    //ACHIEVEMENT ACTION
    partial class AchievementAction
    {
        public static List<AchievementAction> GetList(int platformId)
        {
            List<AchievementAction> list = new List<AchievementAction>();

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                list = (
                    from ach in dbContext.AchievementActions
                    where ach.platformId == platformId
                    select ach
                ).ToList();
            }

            return list;
        }

        public static List<ActionResult> GetResultList(int platformId, string languageCode)
        {
            List<AchievementAction> achActions = GetList(platformId);
            List<ActionResult> actions = new List<ActionResult>();
            foreach (AchievementAction action in achActions)
            {
                AchievementActionText text = AchievementActionText.Get(action.actionId, languageCode);
                actions.Add(new ActionResult()
                {
                    actionCode = action.actionCode,
                    name = action.name,
                    description = text != null ? text.name : action.description,
                    points = action.points
                });
            }
            return actions;
        }

        public static AchievementAction GetByID(int actionId)
        {
            AchievementAction action = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                IQueryable<AchievementAction> actions = dbContext.AchievementActions.Where(r => r.actionId == actionId);
                if (actions.Any())
                {
                    action = actions.ToList().First();
                }
            }

            return action;
        }

        public static AchievementAction GetByActionCode(int platformId, string actionCode)
        {
            AchievementAction action = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                IQueryable<AchievementAction> actions = dbContext.AchievementActions.Where(r => r.platformId == platformId && r.actionCode == actionCode);
                if (actions.Any())
                {
                    action = actions.ToList().First();
                }
            }

            return action;
        }
    }

    //ACHIEVEMENT ACTION TEXT
    partial class AchievementActionText
    {
        public static AchievementActionText Get(int actionId, string languageCode)
        {
            AchievementActionText text = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                Language lang = Language.Get(languageCode);
                IQueryable<AchievementActionText> texts = dbContext.AchievementActionTexts.Where(r => r.actionId == actionId && r.languageId == lang.languageId);
                if (texts.Any())
                {
                    text = texts.ToList().First();
                }
            }
            return text;
        }
    }

    //ACHIEVEMENT ACTION LOG
    partial class AchievementActionLog
    {
        public static List<AchievementActionLog> GetList(int platformId, int profileId)
        {
            List<AchievementActionLog> list = new List<AchievementActionLog>();

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                list = (
                    from al in dbContext.AchievementActionLogs
                    where al.platformId == platformId && al.profileId == profileId
                    select al
                ).ToList();
            }

            return list;
        }

        public static ProfileResult commitActions(int platformId, int profileId, List<ActionLog> actions, string languageCode)
        {
            //Get current profile result to compare after commit
            ProfileResult previousResult = ProfileAchievement.GetResult(platformId, profileId, languageCode);

            //List<AchievementAction> successActions = new List<AchievementAction>();
            List<string> resultLog = new List<string>();

            //Process actions to DB
            foreach (ActionLog action in actions)
            {
                AchievementAction achAction = AchievementAction.GetByActionCode(platformId, action.actionCode);
                if (achAction == null)
                {
                    resultLog.Add(String.Format("Error: actionCode {0} does not exsist, action not Committed \n", action.actionCode));
                    continue;
                }

                AchievementActionLog newLog = new AchievementActionLog
                {
                    platformId = platformId,
                    profileId = profileId,
                    actionId = achAction.actionId,
                    value = String.IsNullOrEmpty(action.value) ? "" : action.value,
                    points = achAction.points
                };

                try
                {
                    int logId = Create(newLog);
                    //successActions.Add(achAction);
                    resultLog.Add(String.Format("Success: action with actionCode {0} Committed \n", action.actionCode));
                }
                catch (Exception exp)
                {
                    resultLog.Add(String.Format("DBError: actionCode {0} exeption: {1} \n", action.actionCode, exp.Message));
                }
            }

            //Process results to DB
            ProfileAchievement.Process(platformId, profileId);

            //Get new profile result
            ProfileResult newResult = ProfileAchievement.GetResult(platformId, profileId, languageCode);

            //check new achievements successes
            List<AchievementResult> previousAchs = previousResult.achievements.Where(a => a.percentage == 100).ToList();
            List<AchievementResult> currentAchs = newResult.achievements.Where(a => a.percentage == 100).ToList();
            List<AchievementResult> newAchs = currentAchs.Where(a => previousAchs.Find(b => b.achievementId == a.achievementId) == null).ToList();
            if (newAchs.Any())
            {
                //newResult.successAchievements = newAchs;
                AchievementResult successChallenge = newAchs.Find(a => a.isChallenge == true);
                if (successChallenge != null)
                {
                    successChallenge.rules = AchievementRule.GetResultList(platformId, profileId, successChallenge.achievementId, languageCode);
                    newResult.successChallenge = successChallenge;
                }
            }

            newResult.resultLog = resultLog;

            return newResult;
        }

        public static int Create(AchievementActionLog newLog)
        {
            int result = 0;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                newLog.createdOn = DateTime.Now;
                dbContext.AchievementActionLogs.InsertOnSubmit(newLog);
                dbContext.SubmitChanges();
                result = newLog.logId;
            }

            return result;
        }

        public static int GetPoints(int platformId, int profileId)
        {
            int points = 0;
            List<AchievementActionLog> logList = GetList(platformId, profileId);
            if (logList.Any())
            {
                points = logList.Sum(al => al.points);
            }
            return points;
        }

        public static List<ScoreResult> GetQuarterResult(int platformId, int profileId, DateTime startDate, DateTime endDate, string languageCode)
        {
            List<ScoreResult> result = new List<ScoreResult>();
            List<AchievementActionLog> logList = GetList(platformId, profileId);
            logList = logList.Where(r => r.createdOn.CompareTo(startDate) >= 0 && r.createdOn.CompareTo(endDate) < 0).ToList();

            if (logList.Any())
            {
                List<AchievementAction> actionList = AchievementAction.GetList(platformId);
                foreach (AchievementAction action in actionList)
                {
                    List<AchievementActionLog> actionLogs = logList.Where(r => r.actionId == action.actionId).ToList();
                    int points = actionLogs.Sum(r => r.points);
                    if (points > 0)
                    {
                        AchievementActionText actionText = AchievementActionText.Get(action.actionId, languageCode);
                        ActionResult actionResult = new ActionResult() { actionCode = action.actionCode, name = action.name, description = actionText != null ? actionText.name : "", count = actionLogs.Count, points = points };
                        ScoreResult scoreResult = new ScoreResult() { actionResult = actionResult };
                        result.Add(scoreResult);
                    }
                }

            }
            return result;
        }

        public static int GetActionCount(int platformId, int profileId, List<AchievementActionLog> logList, AchievementRule rule)
        {
            List<AchievementActionLog> actionLogs = logList.FindAll(r => r.actionId == rule.actionId);

            //initial Count
            int logCount = actionLogs.Count;

            //distinct Value Count
            if (rule.countMustBeDistinct)
            {
                if (rule.useDistinctMaxCount)
                {
                    using (AchievementsDataContext dbContext = new AchievementsDataContext())
                    {
                        //System.Data.Linq.ISingleResult<System.Data.DataTable> tet = dbContext.ActionLog_GetDistinctMaxCount(platformId, profileId, rule.actionId);
                        logCount = (int)dbContext.ActionLog_GetDistinctMaxCount(platformId, profileId, rule.actionId).First().valueCount;
                    }
                }
                else
                {
                    IEnumerable<AchievementActionLog> distinctLogs = actionLogs.Distinct(new LogValueComparer());
                    logCount = distinctLogs.Count();
                }
            }

            //specific Value Count
            else if (String.IsNullOrEmpty(rule.specificValue) == false)
            {
                List<AchievementActionLog> specificLogs = actionLogs.FindAll(al => al.value == rule.specificValue);
                logCount = specificLogs.Count;
            }

            return logCount;
        }

    }

    //PROFILE ACHIEVEMENT
    partial class ProfileMap
    {

        public static int GetMappedId(int platformId, string profileId)
        {

            int result = -1;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                IQueryable<ProfileMap> pMap = dbContext.ProfileMaps.Where(r => r.platformId == platformId && r.profileId == profileId);
                if (pMap.Any())
                {
                    result = pMap.ToList().First().profileMapId;
                }
                else
                {
                    result = Create(platformId, profileId);
                }
            }

            return result;
        }

        public static int Create(int platformId, string profileId)
        {
            int result = -1;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                ProfileMap newMap = new ProfileMap() { platformId = platformId, profileId = profileId };
                dbContext.ProfileMaps.InsertOnSubmit(newMap);
                dbContext.SubmitChanges();
                result = newMap.profileMapId;
            }

            return result;
        }
    }

    //PROFILE ACHIEVEMENT
    partial class ProfileAchievement
    {
        public static List<ProfileAchievement> GetList(int platformId, int profileId)
        {
            List<ProfileAchievement> list = new List<ProfileAchievement>();

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                list = (
                    from r in dbContext.ProfileAchievements
                    where r.platformId == platformId && r.profileId == profileId
                    select r
                ).ToList();
            }

            return list;
        }

        public static ProfileResult GetResult(int platformId, int profileId, string languageCode)
        {
            ProfileResult result = new ProfileResult() { achievements = new List<AchievementResult>() };

            List<Achievement> achList = Achievement.GetList(platformId);
            List<ProfileAchievement> paList = GetList(platformId, profileId);

            int lastChallengeOrderId = 0;
            foreach (ProfileAchievement pa in paList)
            {
                Achievement ach = achList.Find(r => r.achievementId == pa.achievementId);
                AchievementTextResult achText = AchievementText.GetResult(ach.achievementId, languageCode);
                if (pa.confirmedOn.HasValue || ach.isChallenge == false)
                {
                    result.achievements.Add(new AchievementResult() { achievementId = ach.achievementId, name = ach.name, text = achText, bonusPoints = ach.bonusPoints, isChallenge = ach.isChallenge, isConfirmed = pa.confirmedOn.HasValue, orderId = ach.orderId, percentage = pa.percentage });
                }
                else if (ach.isChallenge)
                {
                    result.currentChallenge = new AchievementResult() { achievementId = ach.achievementId, name = ach.name, text = achText, bonusPoints = ach.bonusPoints, isChallenge = ach.isChallenge, isConfirmed = pa.confirmedOn.HasValue, orderId = ach.orderId, percentage = pa.percentage };
                }

                if (ach.isChallenge) { lastChallengeOrderId = ach.orderId; };
            }

            if (result.currentChallenge == null)
            {
                Achievement ach = achList.Find(r => r.orderId > lastChallengeOrderId);
                if (ach != null)
                {
                    AchievementTextResult achText = AchievementText.GetResult(ach.achievementId, languageCode);
                    result.currentChallenge = new AchievementResult() { achievementId = ach.achievementId, name = ach.name, text = achText, bonusPoints = ach.bonusPoints, isChallenge = ach.isChallenge, isConfirmed = false, orderId = ach.orderId, percentage = 0 };
                }
            }

            //add rules for current challenge
            if (result.currentChallenge != null)
            {
                result.currentChallenge.rules = AchievementRule.GetResultList(platformId, profileId, result.currentChallenge.achievementId, languageCode);
            }

            result.actionPoints = AchievementActionLog.GetPoints(platformId, profileId);
            result.rulePoints = ProfileAchievementRule.GetPoints(platformId, profileId);
            result.bonusPoints = GetBonusPoints(platformId, profileId);
            result.totalPoints = result.actionPoints + result.rulePoints + result.bonusPoints;

            result.yearResult = GetYearResult(platformId, profileId, languageCode);

            result.profileInfo = ProfileLog.GetResult(platformId, profileId);

            return result;
        }

        public static ProfileAchievement GetByAchievementID(int platformId, int profileId, int achievementId)
        {
            ProfileAchievement pAch = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                try
                {
                    pAch = dbContext.ProfileAchievements.Single(r => r.platformId == platformId && r.profileId == profileId && r.achievementId == achievementId);
                }
                catch (Exception exp) { }
            }
            return pAch;
        }

        public static void Save(int platformId, int profileId, int achievementId, int percentage)
        {
            ProfileAchievement pAch = GetByAchievementID(platformId, profileId, achievementId);

            if (pAch != null && pAch.percentage == 100)
            {
                return;
            }

            percentage = Math.Min(percentage, 100);

            if (pAch == null)
            {
                using (AchievementsDataContext dbContext = new AchievementsDataContext())
                {
                    pAch = new ProfileAchievement { platformId = platformId, profileId = profileId, achievementId = achievementId, percentage = percentage, createdOn = DateTime.Now };
                    if (percentage == 100)
                    {
                        pAch.completedOn = DateTime.Now;
                    }
                    dbContext.ProfileAchievements.InsertOnSubmit(pAch);
                    dbContext.SubmitChanges();
                }
            }
            else
            {
                pAch.percentage = percentage;
                pAch.Save();
            }
        }

        public void Save()
        {
            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                ProfileAchievement pAch = dbContext.ProfileAchievements.Single(p => p.profileAchievementId == profileAchievementId);
                pAch.percentage = percentage;
                if (percentage == 100 && pAch.completedOn.HasValue == false)
                {
                    pAch.completedOn = DateTime.Now;
                }
                if (confirmedOn.HasValue && pAch.confirmedOn.HasValue == false)
                {
                    pAch.confirmedOn = DateTime.Now;
                }
                dbContext.SubmitChanges();
            }
        }

        public static ProfileResult confirmChallenge(int platformId, int profileId, int achievementId, string languageCode)
        {
            ProfileAchievement pAch = GetByAchievementID(platformId, profileId, achievementId);

            if (pAch != null && pAch.percentage == 100)
            {
                if (pAch.confirmedOn.HasValue == false)
                {
                    pAch.confirmedOn = DateTime.Now;
                }
                pAch.Save();
            }

            ProfileResult newResult = ProfileAchievement.GetResult(platformId, profileId, languageCode);
            return newResult;
        }

        public static void Process(int platformId, int profileId)
        {
            List<Achievement> allAchievements = Achievement.GetList(platformId);
            List<ProfileAchievement> allProfileAchievements = ProfileAchievement.GetList(platformId, profileId);
            List<AchievementActionLog> logList = AchievementActionLog.GetList(platformId, profileId);

            foreach (Achievement ach in allAchievements)
            {
                ProfileAchievement pAch = allProfileAchievements.Find(l => l.achievementId == ach.achievementId);
                if (pAch != null && pAch.percentage == 100)
                {
                    continue;
                }

                if (ach.isChallenge)
                {
                    //check if previous challenge exsists
                    Achievement achPrevious = Achievement.findPreviousChallenge(platformId, ach.achievementId);
                    if (achPrevious == null)
                    {
                        CheckAccomplished(platformId, profileId, logList, ach);
                    }
                    //check if previous challenge is accomplished and confirmed
                    else
                    {
                        ProfileAchievement pAchPrev = allProfileAchievements.Find(l => l.achievementId == achPrevious.achievementId);
                        if (pAchPrev != null && pAchPrev.percentage == 100 && pAchPrev.confirmedOn.HasValue)
                        {
                            CheckAccomplished(platformId, profileId, logList, ach, pAchPrev);
                        }
                    }
                }
                else
                {
                    CheckAccomplished(platformId, profileId, logList, ach);
                }
            }
        }

        private static void CheckAccomplished(int platformId, int profileId, List<AchievementActionLog> logList, Achievement ach, ProfileAchievement previousChallenge = null)
        {
            if (ach.isActive == false)
            {
                return;
            }

            List<AchievementRule> ruleList = AchievementRule.GetList(ach.achievementId);
            if (ruleList.Any() == false)
            {
                return;
            }

            int targetAmount = ruleList.Sum(r => r.amount);
            int totalCount = 0;

            //if challenge has previous challenge only take actions that were logged after previous challenge is accomplished
            if (ach.isChallenge && previousChallenge != null)
            {
                logList = logList.Where(r => r.createdOn.CompareTo(previousChallenge.confirmedOn) > 0).ToList();
            }

            foreach (AchievementRule ar in ruleList)
            {
                int logCount = AchievementActionLog.GetActionCount(platformId, profileId, logList, ar);

                if (logCount < ar.amount)
                {
                    totalCount += logCount;
                }
                else
                {
                    totalCount += ar.amount;
                }
                if (logCount > 0)
                {
                    ProfileAchievementRule.Save(platformId, profileId, ar.ruleId, logCount);
                }
            }

            double fraction = (double)totalCount / targetAmount;
            int perc = Convert.ToInt32(Math.Round(100 * fraction));

            if (perc > 0)
            {
                Save(platformId, profileId, ach.achievementId, perc);
            }

        }

        public static int GetBonusPoints(int platformId, int profileId)
        {
            int points = 0;
            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                List<Achievement> aList = (
                    from pa in dbContext.ProfileAchievements
                    from a in dbContext.Achievements
                    where pa.platformId == platformId && pa.profileId == profileId && pa.percentage == 100 && a.achievementId == pa.achievementId
                    select a
                ).ToList();
                points = aList.Sum(a => a.bonusPoints);
            }
            return points;
        }

        public static YearResult GetYearResult(int platformId, int profileId, string languageCode)
        {
            YearResult result = new YearResult();

            //get last 4 quarters
            for (int i = 0; i <= 9; i += 3)
            {
                DateTime curDate = DateTime.Now.AddMonths(-i);
                int curQuarter = GetQuarter(curDate);
                DateTime startDate = GetQuarterStartingDate(curDate);
                DateTime endDate = startDate.AddMonths(3);

                switch (curQuarter)
                {
                    case 1:
                        result.quarter1 = GetQuarterResult(platformId, profileId, startDate, endDate, languageCode);
                        result.quarter1 = result.quarter1.Concat(ProfileAchievementRule.GetQuarterResult(platformId, profileId, startDate, endDate, languageCode)).ToList();
                        result.quarter1 = result.quarter1.Concat(AchievementActionLog.GetQuarterResult(platformId, profileId, startDate, endDate, languageCode)).ToList();
                        //add other results
                        break;
                    case 2:
                        result.quarter2 = GetQuarterResult(platformId, profileId, startDate, endDate, languageCode);
                        result.quarter2 = result.quarter2.Concat(ProfileAchievementRule.GetQuarterResult(platformId, profileId, startDate, endDate, languageCode)).ToList();
                        result.quarter2 = result.quarter2.Concat(AchievementActionLog.GetQuarterResult(platformId, profileId, startDate, endDate, languageCode)).ToList();
                        //add other results
                        break;
                    case 3:
                        result.quarter3 = GetQuarterResult(platformId, profileId, startDate, endDate, languageCode);
                        result.quarter3 = result.quarter3.Concat(ProfileAchievementRule.GetQuarterResult(platformId, profileId, startDate, endDate, languageCode)).ToList();
                        result.quarter3 = result.quarter3.Concat(AchievementActionLog.GetQuarterResult(platformId, profileId, startDate, endDate, languageCode)).ToList();
                        //add other results
                        break;
                    case 4:
                        result.quarter4 = GetQuarterResult(platformId, profileId, startDate, endDate, languageCode);
                        result.quarter4 = result.quarter4.Concat(ProfileAchievementRule.GetQuarterResult(platformId, profileId, startDate, endDate, languageCode)).ToList();
                        result.quarter4 = result.quarter4.Concat(AchievementActionLog.GetQuarterResult(platformId, profileId, startDate, endDate, languageCode)).ToList();
                        //add other results
                        break;
                }
            }

            return result;
        }

        public static List<ScoreResult> GetQuarterResult(int platformId, int profileId, DateTime startDate, DateTime endDate, string languageCode)
        {
            List<ScoreResult> result = new List<ScoreResult>();
            List<ProfileAchievement> pachList = GetList(platformId, profileId);
            pachList = pachList.Where(r => r.completedOn != null && r.completedOn.GetValueOrDefault().CompareTo(startDate) >= 0 && r.completedOn.GetValueOrDefault().CompareTo(endDate) < 0).ToList();

            if (pachList.Any())
            {
                foreach (ProfileAchievement pach in pachList)
                {
                    Achievement ach = Achievement.GetByID(pach.achievementId);
                    AchievementTextResult achText = AchievementText.GetResult(ach.achievementId, languageCode);
                    AchievementResult achResult = new AchievementResult() { achievementId = ach.achievementId, name = ach.name, text = achText, bonusPoints = ach.bonusPoints, isChallenge = ach.isChallenge, isConfirmed = pach.confirmedOn.HasValue };
                    ScoreResult scoreResult = new ScoreResult() { achievementResult = achResult };
                    result.Add(scoreResult);
                }

            }
            return result;
        }

        public static int GetQuarter(DateTime myDate)
        {
            return Convert.ToInt32(Math.Ceiling(myDate.Month / 3.0));
        }

        public static DateTime GetQuarterStartingDate(DateTime myDate)
        {
            return new DateTime(myDate.Year, (3 * GetQuarter(myDate)) - 2, 1);
        }
    }

    //PROFILE ACHIEVEMENT RULE
    partial class ProfileAchievementRule
    {
        public static List<ProfileAchievementRule> GetList(int platformId, int profileId)
        {
            List<ProfileAchievementRule> list = new List<ProfileAchievementRule>();

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                list = (
                    from r in dbContext.ProfileAchievementRules
                    where r.platformId == platformId && r.profileId == profileId
                    select r
                ).ToList();
            }

            return list;
        }

        public static ProfileAchievementRule GetByRuleID(int platformId, int profileId, int ruleId)
        {
            ProfileAchievementRule par = null;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                try
                {
                    par = dbContext.ProfileAchievementRules.Single(r => r.platformId == platformId && r.profileId == profileId && r.ruleId == ruleId);
                }
                catch (Exception exp) { }
            }
            return par;
        }

        public static List<ScoreResult> GetQuarterResult(int platformId, int profileId, DateTime startDate, DateTime endDate, string languageCode)
        {
            List<ScoreResult> result = new List<ScoreResult>();
            List<ProfileAchievementRule> parList = GetList(platformId, profileId);
            parList = parList.Where(r => r.completedOn != null && r.completedOn.GetValueOrDefault().CompareTo(startDate) >= 0 && r.completedOn.GetValueOrDefault().CompareTo(endDate) < 0).ToList();

            if (parList.Any())
            {
                foreach (ProfileAchievementRule par in parList)
                {

                    AchievementRule ar = AchievementRule.GetByID(par.ruleId);
                    Achievement ach = Achievement.GetByID(ar.achievementId);
                    if (ach.isChallenge)
                    {
                        AchievementText achText = AchievementText.Get(ar.achievementId, languageCode);
                        AchievementAction action = AchievementAction.GetByID(ar.actionId);
                        AchievementActionText actionText = AchievementActionText.Get(action.actionId, languageCode);

                        ActionResult ruleAction = new ActionResult() { actionCode = action.actionCode, name = action.name, description = actionText.name };
                        RuleResult ruleResult = new RuleResult() { targetCount = ar.amount, ruleAction = ruleAction, rulePoints = ar.points, description = achText.name };
                        ScoreResult scoreResult = new ScoreResult() { ruleResult = ruleResult };
                        result.Add(scoreResult);
                    }
                }

            }
            return result;
        }

        public static void Save(int platformId, int profileId, int ruleId, int count)
        {
            ProfileAchievementRule par = GetByRuleID(platformId, profileId, ruleId);
            AchievementRule ar = AchievementRule.GetByID(ruleId);

            count = Math.Min(count, ar.amount);
            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                if (par == null)
                {
                    par = new ProfileAchievementRule { platformId = platformId, profileId = profileId, ruleId = ruleId, count = count, createdOn = DateTime.Now };
                    if (count == ar.amount)
                    {
                        par.completedOn = DateTime.Now;
                    }
                    dbContext.ProfileAchievementRules.InsertOnSubmit(par);
                    dbContext.SubmitChanges();
                }
                else
                {
                    par.count = count;
                    if (count == ar.amount)
                    {
                        par.completedOn = DateTime.Now;
                    }
                    par.Save();
                }

            }
        }

        public void Save()
        {
            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                ProfileAchievementRule par = dbContext.ProfileAchievementRules.Single(p => p.platformId == platformId && p.profileId == profileId && p.ruleId == ruleId);
                AchievementRule ar = AchievementRule.GetByID(ruleId);
                par.count = count;
                if (count == ar.amount)
                {
                    par.completedOn = DateTime.Now;
                }
                dbContext.SubmitChanges();
            }
        }

        public static int GetPoints(int platformId, int profileId)
        {
            int points = 0;
            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                List<AchievementRule> arList = (
                    from r in dbContext.ProfileAchievementRules
                    from ar in dbContext.AchievementRules
                    where r.platformId == platformId && r.profileId == profileId && r.completedOn != null && ar.ruleId == r.ruleId
                    select ar
                ).ToList();
                points = arList.Sum(ar => ar.points);
            }
            return points;
        }
    }

    //PROFILE LOG
    partial class ProfileLog
    {
        public static List<ProfileLog> GetList(int platformId, int profileId)
        {
            List<ProfileLog> list = new List<ProfileLog>();

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                list = (
                    from pl in dbContext.ProfileLogs
                    where pl.platformId == platformId && pl.profileId == profileId
                    select pl
                ).ToList();
            }

            return list;
        }

        public static List<ProfileLogResult> GetResult(int platformId, int profileId)
        {
            List<ProfileLogResult> result = new List<ProfileLogResult>();
            List<ProfileLog> list = GetList(platformId, profileId);

            foreach (ProfileLog log in list)
            {
                ProfileLogResult logResult = new ProfileLogResult() { logKey = log.logKey, logValue = log.logValue };
                result.Add(logResult);
            }

            return result;
        }

        public static void commitLogs(int platformId, int profileId, List<ProfileLogResult> logs)
        {
            //Process actions to DB
            foreach (ProfileLogResult log in logs)
            {
                ProfileLog newLog = new ProfileLog
                {
                    platformId = platformId,
                    profileId = profileId,
                    logKey = log.logKey,
                    logValue = log.logValue
                };

                try
                {
                    int logId = Create(newLog);
                }
                catch (Exception exp)
                {
                }
            }
        }

        public static int Create(ProfileLog newLog)
        {
            int result = 0;

            using (AchievementsDataContext dbContext = new AchievementsDataContext())
            {
                newLog.createdOn = DateTime.Now;
                dbContext.ProfileLogs.InsertOnSubmit(newLog);
                dbContext.SubmitChanges();
                result = newLog.logId;
            }

            return result;
        }
    }

    //HELPER CLASSES

    // Custom comparer for the AchievementActionLog class. 
    class LogValueComparer : IEqualityComparer<AchievementActionLog>
    {
        // Logs are equal if their value fields are equal. 
        public bool Equals(AchievementActionLog x, AchievementActionLog y)
        {
            // Check whether the Actionlog's value properties are equal. 
            return x.value == y.value;
        }

        //If Equals() returns true for a pair of objects, 
        //GetHashCode must return the same value for these objects. 
        public int GetHashCode(AchievementActionLog log)
        {
            // Check whether the object is null. 
            if (Object.ReferenceEquals(log, null)) return 0;

            // Get the hash code for the value field if it is not null. 
            int hashLogValue = log.value == null ? 0 : log.value.GetHashCode();

            // Calculate the hash code for the product. 
            return hashLogValue;
        }

    }


    //SERIALIZER CLASSES

    //changed to result classes (because System.ObjectDisposedException with serialization)
    [DataContract]
    public class ProfileResult
    {
        [DataMember]
        public List<AchievementResult> achievements { get; set; }
        [DataMember]
        public AchievementResult currentChallenge { get; set; }
        [DataMember]
        public int actionPoints { get; set; }
        [DataMember]
        public int rulePoints { get; set; }
        [DataMember]
        public int bonusPoints { get; set; }
        [DataMember]
        public int totalPoints { get; set; }
        //[DataMember]
        //public List<AchievementResult> successAchievements { get; set; }
        [DataMember]
        public AchievementResult successChallenge { get; set; }
        [DataMember]
        public YearResult yearResult { get; set; }
        [DataMember]
        public List<ProfileLogResult> profileInfo { get; set; }
        [DataMember]
        public List<string> resultLog { get; set; }
    }

    [DataContract]
    public class AchievementResult
    {
        [DataMember]
        public int achievementId { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public AchievementTextResult text { get; set; }
        [DataMember]
        public bool isChallenge { get; set; }
        [DataMember]
        public bool isConfirmed { get; set; }
        [DataMember]
        public int orderId { get; set; }
        [DataMember]
        public int percentage { get; set; }
        [DataMember]
        public int bonusPoints { get; set; }
        [DataMember]
        public List<RuleResult> rules { get; set; }

    }

    [DataContract]
    public class AchievementTextResult
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string completed { get; set; }
        [DataMember]
        public string reward { get; set; }
    }

    [DataContract]
    public class RuleResult
    {
        [DataMember]
        public ActionResult ruleAction { get; set; }
        [DataMember]
        public int targetCount { get; set; }
        [DataMember]
        public int currentCount { get; set; }
        [DataMember]
        public string specificValue { get; set; }
        [DataMember]
        public int rulePoints { get; set; }
        [DataMember]
        public string description { get; set; }
    }

    [DataContract]
    public class ActionResult
    {
        [DataMember]
        public string actionCode { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public int count { get; set; }
        [DataMember]
        public int points { get; set; }
    }

    [DataContract]
    public class YearResult
    {
        [DataMember]
        public List<ScoreResult> quarter1 { get; set; }
        [DataMember]
        public List<ScoreResult> quarter2 { get; set; }
        [DataMember]
        public List<ScoreResult> quarter3 { get; set; }
        [DataMember]
        public List<ScoreResult> quarter4 { get; set; }
    }

    [DataContract]
    public class ScoreResult
    {
        [DataMember]
        public ActionResult actionResult { get; set; }
        [DataMember]
        public AchievementResult achievementResult { get; set; }
        [DataMember]
        public RuleResult ruleResult { get; set; }
    }

    [DataContract]
    public class ActionLog
    {
        [DataMember]
        public string actionCode { get; set; }
        [DataMember]
        public string value { get; set; }
        [DataMember]
        public int points { get; set; }
    }

    [DataContract]
    public class ProfileLogResult
    {
        [DataMember]
        public string logKey { get; set; }
        [DataMember]
        public string logValue { get; set; }
    }
}

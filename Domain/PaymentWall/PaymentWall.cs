﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.PaymentWall
{
    class PaymentWall
    {
    }

    partial class s2m_log_paymentWall {

        public static int Create(s2m_log_paymentWall newLog)
        {
            int result = 0;

            using (PaymentWallDataContext dbContext = new PaymentWallDataContext())
            {
                newLog.createdOn = DateTime.Now;
                dbContext.s2m_log_paymentWalls.InsertOnSubmit(newLog);
                dbContext.SubmitChanges();
                result = newLog.logId;
            }

            return result;
        }

        public static List<s2m_log_paymentWall> GetList(DateTime startDate, DateTime endDate ) {

            List<s2m_log_paymentWall> logList = new List<s2m_log_paymentWall>();

            using (PaymentWallDataContext dbContext = new PaymentWallDataContext())
            {
                logList = dbContext.s2m_log_paymentWalls.Where(r => r.createdOn.CompareTo(startDate) >= 0 && r.createdOn.CompareTo(endDate) < 0).ToList();
            }

            return logList;
        }

        public static List<s2m_log_paymentWall> GetUserList(string userId)
        {

            List<s2m_log_paymentWall> logList = new List<s2m_log_paymentWall>();

            using (PaymentWallDataContext dbContext = new PaymentWallDataContext())
            {
                logList = dbContext.s2m_log_paymentWalls.Where(r => r.userId == userId).ToList();
            }

            return logList;
        }
    }
}

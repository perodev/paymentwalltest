﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Domain.Perks
{
    class Perks
    {
    }

    partial class s2m_perk 
    {

        public static s2m_perk Get(int perkId)
        {

            s2m_perk perk = null;

            using (PerksDataContext dbContext = new PerksDataContext())
            {
                IQueryable<s2m_perk> perks = dbContext.s2m_perks.Where(r => r.perkId == perkId);
                if (perks.Any())
                {
                    perk = perks.ToList().First();
                }
            }

            return perk;
        }

        public static List<s2m_perk> GetList()
        {

            List<s2m_perk> list = new List<s2m_perk>();

            using (PerksDataContext dbContext = new PerksDataContext())
            {
                list = dbContext.s2m_perks.ToList();
            }

            return list;
        }
    }

    partial class s2m_upsell_perk
    {
        public static List<s2m_upsell_perk> GetList()
        {

            List<s2m_upsell_perk> list = new List<s2m_upsell_perk>();

            using (PerksDataContext dbContext = new PerksDataContext())
            {
                list = dbContext.s2m_upsell_perks.ToList();
            }

            return list;
        }

        public static List<PerkResult> GetResultList()
        {

            List<PerkResult> resultList = new List<PerkResult>();
            List<s2m_upsell_perk> list = GetList();

            foreach (s2m_upsell_perk upsellPerk in list)
            {
                s2m_perk perk = s2m_perk.Get(upsellPerk.perkId);
                resultList.Add(new PerkResult() { perkCode = perk.perkCode, description = perk.description, upsellProductCode = upsellPerk.productCode, achievementCode = upsellPerk.requiredAchievement });
            }

            return resultList;
        }

    }

    partial class s2m_achievement_perk
    {
        public static List<s2m_achievement_perk> GetList()
        {

            List<s2m_achievement_perk> list = new List<s2m_achievement_perk>();

            using (PerksDataContext dbContext = new PerksDataContext())
            {
                list = dbContext.s2m_achievement_perks.ToList();
            }

            return list;
        }

        public static List<PerkResult> GetResultList()
        {

            List<PerkResult> resultList = new List<PerkResult>();
            List<s2m_achievement_perk> list = GetList();

            foreach (s2m_achievement_perk achPerk in list)
            {
                s2m_perk perk = s2m_perk.Get(achPerk.perkId);
                resultList.Add(new PerkResult() { perkCode = perk.perkCode, description = perk.description, achievementCode = achPerk.achievementCode });
            }

            return resultList;
        }

    }

    partial class s2m_profile_perk
    {
        public static List<s2m_profile_perk> GetList(int profileId)
        {

            List<s2m_profile_perk> list = new List<s2m_profile_perk>();

            using (PerksDataContext dbContext = new PerksDataContext())
            {
                list = dbContext.s2m_profile_perks.Where(r => r.profileId == profileId).ToList();
            }

            return list;
        }

    }

    public class PerkResult
    {
        public string perkCode { get; set; }
        public string description { get; set; }
        public string upsellProductCode { get; set; }
        public string achievementCode { get; set; }
    }
}

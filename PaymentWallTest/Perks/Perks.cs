﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Domain.Perks;
using Domain.Achievements;
using PaymentWallTest.PaymentWall;

namespace PaymentWallTest.Perks
{
    public class Perks
    {
        static int platformId = 1;

        public static bool UserHasPerk(string userId, string perkCode)
        {
            List<PerkResult> userPerks = GetUserPerks(userId);
            PerkResult foundPerk = userPerks.Find(r => r.perkCode == perkCode);
            return foundPerk != null;
        }

        public static List<PerkResult> GetUserPerks(string userId)
        {
            int mappedProfileId = ProfileMap.GetMappedId(platformId, userId);
            ProfileResult profileResult = ProfileAchievement.GetResult(platformId, mappedProfileId, "nl");//Dit poorten naar profile_achievement op dev DB
            
            List<PerkResult> userPerks = new List<PerkResult>();

            List<s2m_perk> allPerks = s2m_perk.GetList();
            List<s2m_achievement_perk> achPerks = s2m_achievement_perk.GetList();
            List<s2m_upsell_perk> upsellPerks = s2m_upsell_perk.GetList();

            List<SubscriptionResult> userSubscriptions = PaymentWall.PaymentWall.GetValidSubscriptions(userId);


            foreach (s2m_perk perk in allPerks)
            {

                //check achievement perks
                s2m_achievement_perk achPerk = achPerks.Find(r => r.perkId == perk.perkId);
                if (achPerk != null) {
                    AchievementResult userAch = profileResult.achievements.Find(r => r.name == achPerk.achievementCode && r.percentage == 100);
                    if (userAch != null) {
                        userPerks.Add(new PerkResult() { perkCode = perk.perkCode, description = perk.description, achievementCode = achPerk.achievementCode});
                    }
                }

                //check upsell perks
                s2m_upsell_perk upsellPerk = upsellPerks.Find(r => r.perkId == perk.perkId);
                if (upsellPerk != null)
                {
                    SubscriptionResult userSub = userSubscriptions.Find(r => r.productCode.IndexOf(upsellPerk.productCode) > -1);
                    if (userSub != null)
                    {
                        userPerks.Add(new PerkResult() { perkCode = perk.perkCode, description= perk.description, upsellProductCode = upsellPerk.productCode });
                    }
                }
            }

            return userPerks;

        }

    }
}
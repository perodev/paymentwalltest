﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Domain.Perks;
using Domain.Achievements;
using PaymentWallTest.PaymentWall;
using PaymentWallTest.Perks;
using S2M.Resources;

namespace PaymentWallTest
{
    public partial class Upsell : System.Web.UI.Page
    {

        string userId;
        int platformId = 1;

        ProfileResult profileResult;


        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Request["uid"];
            if (!string.IsNullOrEmpty(userId)) {
                TextBox_ProfileID.Text = userId;//TEST
            }
            SetSubscriptions();
        }

        protected void SetSubscriptions() {

            userId = TextBox_ProfileID.Text;//TEST
            ShowUserPerks(userId); //TEST

            int mappedProfileId = ProfileMap.GetMappedId(platformId, userId);
            profileResult = ProfileAchievement.GetResult(platformId, mappedProfileId, "nl");//Dit poorten naar profile_achievement op dev DB

            List<PerkResult> perks = s2m_upsell_perk.GetResultList();
            List<SubscriptionResult> userSubscriptions = PaymentWall.PaymentWall.GetValidSubscriptions(userId);

            ContentPlaceHolder myContent = (ContentPlaceHolder)this.Master.FindControl("MainContent");
                
            foreach (PerkResult perk in perks)
	        {
                HtmlGenericControl upsellDiv = myContent.FindControl(perk.upsellProductCode) as HtmlGenericControl;

                if (upsellDiv != null)
                {
                    HtmlGenericControl butDiv = upsellDiv.FindControl(perk.upsellProductCode + "_but") as HtmlGenericControl;
                    HtmlGenericControl descDiv = upsellDiv.FindControl(perk.upsellProductCode + "_desc") as HtmlGenericControl;
                    HtmlGenericControl iconDiv = upsellDiv.FindControl(perk.upsellProductCode + "_icon") as HtmlGenericControl;

                    descDiv.InnerHtml = S2M.Resources.Upsell.Upsell.ResourceManager.GetString(perk.upsellProductCode + "_desc");
                    iconDiv.Style.Add("background-image", "url(/Images/Subscriptions/icon_" + perk.upsellProductCode + ".png)");

                    if (!string.IsNullOrEmpty(perk.achievementCode) && !UserHasAchievement(perk.achievementCode))
                    {
                        upsellDiv.Attributes.Add("class", "s2m_upsell s2m_upsell_locked");
                        butDiv.Visible = false;
                    }
                    else
                    {
                        upsellDiv.Attributes.Add("class", "s2m_upsell s2m_upsell_available");

                        SubscriptionResult userSubscription = userSubscriptions.Find(r => r.productCode.IndexOf(perk.upsellProductCode) > -1);
                        if (userSubscription == null)
                        {
                            butDiv.Visible = true;
                            string requestUrl = "PaymentWall/PaymentRequest.aspx?user=" + userId + "&subscription=" + perk.upsellProductCode;
                            butDiv.Attributes.Add("onclick", "getSubscription(\"" + requestUrl + "\")");
                        }
                        else
                        {
                            descDiv.InnerHtml += "<b><br />U heeft dit abonnement<br /></b>geldig tot: " + userSubscription.expirationDate.ToString();
                            butDiv.Visible = false;
                        }
                    }
                }
                
	        }
            
        }

        protected void ShowUserPerks(string userId)
        {
            List<PerkResult> perkList = Perks.Perks.GetUserPerks(userId);
            RepeaterPerks.DataSource = perkList;
            RepeaterPerks.DataBind();
        }

        protected bool UserHasAchievement(string achievementCode) {
            foreach (AchievementResult achResult in profileResult.achievements) {
                if (achResult.name == achievementCode && achResult.percentage == 100) {
                    return true;
                }
            }
            return false;
        }

        protected void load_btn_Click(object sender, EventArgs e)
        {
            SetSubscriptions();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using Domain.PaymentWall;

namespace PaymentWallTest
{
    public partial class PaymentRequest : System.Web.UI.Page
    {
        public string ErrorMessage = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            string parameters_string = "";
            string signature;

            
            //string applicationKey = "ad89a17584e0c8460bf47cad6da061bf"; 9e643f3e4e9b2869ae7d6f18b63fdd49
            //string secretKey = "d1eb3012fc7378fa65acac93160e9f00"; 2e84cf4ead8fba21c1d77fbabb086c00
            
            //perodevelop@gmail.com Paymentwall account
            string applicationKey = "9e643f3e4e9b2869ae7d6f18b63fdd49";
            string secretKey = "2e84cf4ead8fba21c1d77fbabb086c00"; 

            string widgetKey = "p1_1";

            string userId = Request["user"];
            string subscription = Request["subscription"];
            string period = Request["speriod"];
            string periodlength = Request["slength"];
            string succesURL = "http://localhost:50363/Upsell.aspx?uid=" + userId;
            string defaultGoods = "";
            string[] hideGoods = null;

            //Dit uit config file halen?
            switch (subscription) { 
                case "ABB1":
                    defaultGoods = "ABB1_M";
                    hideGoods = new string[] { "ABB2_M", "ABB2_Y", "ABB3_M", "ABB3_Y", "COMBI1_M", "COMBI1_Y" };
                    break;
                case "ABB2":
                    defaultGoods = "ABB2_M";
                    hideGoods = new string[] { "ABB1_M", "ABB1_Y", "ABB3_M", "ABB3_Y", "COMBI1_M", "COMBI1_Y" };
                    break;
                case "ABB3":
                    defaultGoods = "ABB3_M";
                    hideGoods = new string[] { "ABB1_M", "ABB1_Y", "ABB2_M", "ABB2_Y", "COMBI1_M", "COMBI1_Y" };
                    break;
                case "COMBI1":
                    defaultGoods = "COMBI1_M";
                    hideGoods = new string[] { "ABB1_M", "ABB1_Y", "ABB2_M", "ABB2_Y", "ABB3_M", "ABB3_Y" };
                    break;
            }
            if (widgetKey == null) {
                ErrorMessage = "widgetKey not defined";
                return;
            }

		    try {
                parameters_string += "default_goodsid=" + defaultGoods;
                //parameters_string += "&display_goodsid=" + subscription;
                for (int i = 0; i < hideGoods.Length; i++)
                {
                    parameters_string += "&hide_goodsid[" + i + "]=" + hideGoods[i];
                }
                parameters_string += "&key=" + applicationKey;
                parameters_string +=  "&sign_version=2";
                parameters_string += "&success_url=" + succesURL;
                parameters_string += "&uid=" + userId;
                parameters_string += "&widget=" + widgetKey;

                // signature calculation - md5 ( parameters + secret key );
                //signature = md5(Replace(parameters_string, "&", "") & "[YOUR_SECRET_KEY]");
                using (MD5 md5Hash = MD5.Create())
                {
                    string source = parameters_string.Replace("&", "") + secretKey;
                    signature = GetMd5Hash(md5Hash, source);

                }
                parameters_string +=  "&sign=" + signature;


                periodlength = "-1"; //Dit is nu het geval want keuze wordt op paymentwall gemaakt

                if (string.IsNullOrEmpty(period))
                {
                    period = "unknown";
                }

                if ( period == "unknown" || period == "fixed") 
                { 
                    periodlength = "-1";
                }

                s2m_log_paymentWall newLog = new s2m_log_paymentWall()
                {
                    logType = "request",
                    userId = userId,
                    goodsId = subscription,
                    period = period,
                    periodLength = Convert.ToInt32(periodlength)
                };
                s2m_log_paymentWall.Create(newLog);

                Response.Redirect("https://wallapi.com/api/subscription?" + parameters_string);


                //string srcStr = "https://wallapi.com/api/subscription?" + parameters_string;
                //wallapiIframe.Src = srcStr;
		    }
		    catch (Exception error) {
			    // TODO log error
			    ErrorMessage = error.Message;
		    }

        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }

        // Verify a hash against a string. 
        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input. 
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}


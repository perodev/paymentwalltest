﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using Domain.PaymentWall;

namespace PaymentWallTest
{
    public partial class PaymentPingback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string secretKey = "d1eb3012fc7378fa65acac93160e9f00";
            string[] ipsWhiteList = new string[] { "174.36.92.186", "174.36.96.66", "174.36.92.187", "174.36.92.192", "174.37.14.28" };

            string userId = Request["uid"];
            string goodsId = Request["goodsid"];
            string periodlength = Request["slength"];
            string period = Request["speriod"];
            string pingbackType = Request["type"];
            string chargebackReason = Request["reason"];
            string refId = Request["ref"];
            string signature = Request["sig"];
            string sign_version = Request["sign_version"];


            //    uid=222&
            //    goodsid=DPTEST1&
            //    slength=1&
            //    speriod=month&
            //    type=0&
            //    ref=x0223&
            //    sign_version=2&
            //    sig=fc9685a0917427afebfbbc105c0b7bf1


            //Check required parameters
            if (String.IsNullOrEmpty(userId) || String.IsNullOrEmpty(goodsId) || String.IsNullOrEmpty(pingbackType) || String.IsNullOrEmpty(refId) || String.IsNullOrEmpty(signature))
            {
                Response.Write("Error: Missing parameters!");
                return;
            }
            
            //Check allowed IPS
            string remoteIP = Request.ServerVariables["REMOTE_ADDR"];
            bool validIP = Array.IndexOf(ipsWhiteList, remoteIP) > -1 || remoteIP == "::1" || remoteIP == "127.0.0.1"; //laatste is localhost in IPv6(testdoeleinden)

            if (!validIP) {
                Response.Write("Error: IP not in whitelist! ");
                return;
            }

            //Check signature
            bool result = false;
            if (!string.IsNullOrEmpty(sign_version) && sign_version == "2")
            {
                //handle version 2
                //Not used
            }
            else {
                string signatureParams = "uid=" + userId + "goodsid=" + goodsId + "slength=" + periodlength + "speriod=" + period + "type=" + pingbackType + "ref=" + refId;
                using (MD5 md5Hash = MD5.Create())
                {
                    result = VerifyMd5Hash(md5Hash, signatureParams + secretKey, signature);
                }
            }
            
            if (result)
            {
                if (string.IsNullOrEmpty(period) || period == "fixed")
                {
                    period = "fixed";
                    periodlength = "-1";
                }

                s2m_log_paymentWall newLog = new s2m_log_paymentWall()
                {
                    logType = "pingback",
                    userId = userId,
                    goodsId = goodsId,
                    period = period,
                    periodLength = Convert.ToInt32(periodlength),
                    reference = refId
                };

                if (pingbackType == "2")
                {
                    newLog.status = "chargeback";
                    
                    switch (chargebackReason)
	                {
		                case "1":
                            newLog.statusCode = "Chargeback";
                            break;
                        case "2":
                            newLog.statusCode = "Credit Card fraud"; //Ban user
                            break;
                        case "3":
                            newLog.statusCode = "Order fraud"; //Ban user
                            break;
                        case "4":
                            newLog.statusCode = "Bad data entry";
                            break;
                        case "5":
                            newLog.statusCode = "Fake / proxy user";
                            break;
                        case "6":
                            newLog.statusCode = "Rejected by advertiser";
                            break;
                        case "7":
                            newLog.statusCode = "Duplicate conversions";
                            break;
                        case "8":
                            newLog.statusCode = "Goodwill credit taken back";
                            break;
                        case "9":
                            newLog.statusCode = "Cancelled order"; //refund
                            break;
                        case "10":
                            newLog.statusCode = "Partially reversed transaction";
                            break;
	                }
                }
                else
                {
                    newLog.status = "OK";
                    newLog.statusCode = "Payment Completed";
                    
                }
                s2m_log_paymentWall.Create(newLog);
                Response.Write("OK");
            }
            else
            {
                Response.Write("Error: Signature is not valid!");
            }


        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }

        // Verify a hash against a string. 
        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input. 
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.PaymentWall;

namespace PaymentWallTest.PaymentWall
{
    public class PaymentWall
    {
        public static List<SubscriptionResult> GetValidSubscriptions(string userId)
        {

            List<SubscriptionResult> result = new List<SubscriptionResult>();

            List<s2m_log_paymentWall> logList = s2m_log_paymentWall.GetUserList(userId);
            foreach (s2m_log_paymentWall item in logList)
            {
                if (item.status == "OK") {
                    //check if item has been cancelled and chargeback done
                    s2m_log_paymentWall cancelledItem = logList.Find(r => r.reference == item.reference && r.status == "chargeback");
                    if (cancelledItem != null) { 
                        //don't add item to result
                        continue;
                    }

                    if (item.period == "fixed")
                    {
                        result.Add(new SubscriptionResult() { validSubscription = true, productCode = item.goodsId, period = item.period });
                    }
                    else {
                        //check date of subscription
                        int periodLength = item.periodLength;
                        DateTime endDate = item.createdOn;
                        switch (item.period)
                        {
                            case "month":
                                endDate = endDate.AddMonths(periodLength);
                                break;
                            case "week":
                                endDate = endDate.AddDays(7 * periodLength);
                                break;
                            case "day":
                                endDate = endDate.AddDays(periodLength);
                                break;
                            case "year":
                                endDate = endDate.AddYears(periodLength);
                                break;
                        }
                        if (endDate.CompareTo(DateTime.Now) >= 0)
                        {
                            result.Add(new SubscriptionResult() { validSubscription = true, productCode = item.goodsId, period = item.period, expirationDate = endDate });
                        }
                    }

                }

            }

            return result;
        }

    }

    public class SubscriptionResult
    {
        public string productCode { get; set; }
        public string period { get; set; }
        public bool validSubscription { get; set; }
        public DateTime expirationDate { get; set; }
    }
}
﻿<%@ Page Title="Upsell" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Upsell.aspx.cs" Inherits="PaymentWallTest.Upsell" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <script type="text/javascript">
        function getSubscription(requestUrl) {
            window.location = requestUrl;
        }

    </script>

    <style>
        .s2m_upsell_container {
            color: #ffffff;
            font-family: Sans-serif;
            font-weight: bold;
            text-align:center;
            cursor: default;
        }
        .s2m_upsell {
            position:relative;
            float:left;
            width:230px;
            height:407px;
            background-color:#fff;
            margin-right:21px;
            margin-bottom:21px;
            /*opacity:0.5;*/
        }
        .s2m_upsell_header {
            width:inherit;
            height:76px;
            background-color:#011e2c;
            text-overflow:clip;
        }
        .s2m_upsell_header h1{
            color:#8ad90e;
            font-size:25px;
            font-weight:normal;
            line-height:40px;
            margin:0px;
        }
        .s2m_upsell_header h2{
            font-size:14px;
            margin:0px;
            color:#fff;
        }
        .s2m_upsell_price_month, .s2m_upsell_price_year {
            width:inherit;
            height:30px;
            color:#011e2c;
            line-height:30px;
            font-size:17px;
        }
        .s2m_upsell_price_year {
            background-color:#8ad90e;
        }
        .s2m_upsell_icon {
            width:inherit;
            height:132px;
            background-repeat:no-repeat;
            background-position:center;
        }
        .s2m_upsell_desc {
            width:inherit;
            height:85px;
            color:#011e2c;
            font-size:15px;
            font-weight:normal;
            line-height:25px;
        }
        .s2m_upsell_but {
            width:206px;
            height:43px;
            line-height:43px;
            font-size:20px;
            margin-left:12px;
            background-color:#011e2c;
            cursor:pointer;
        }
        .s2m_upsell_but:hover {
            background-color:#8ad90e;
        }
        .s2m_upsell_surprise {
            position:absolute;
            /*top:136px;*/
            top:0px;
            width:inherit;
            height:inherit;
            /*background-color:#011e2c;*/
            background-image:url(/Images/Subscriptions/locked.png)
        }
        .s2m_upsell_available .s2m_upsell_surprise {
            display:none;
        }
        .s2m_upsell_locked .s2m_upsell_desc, .s2m_upsell_locked .s2m_upsell_icon {
            background-color:#011e2c;
        }
    </style>

    <div>
        <asp:TextBox ID="TextBox_ProfileID" runat="server" Width="150px" Height="20px">testuser1</asp:TextBox>
        <asp:Button ID="load_btn" runat="server" Text="Load Profile" OnClick="load_btn_Click" />
    </div>
    <br />

    <asp:Repeater ID="RepeaterPerks" runat="server" >
        <HeaderTemplate>
        <div>
            <asp:Label ID="Label1" runat="server" Text="perkCode" Font-Bold="True" Width="250" ></asp:Label>
            <asp:Label ID="Label7" runat="server" Text="upsellProductCode" Font-Bold="True" Width="150"  ></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="achievementCode" Font-Bold="True" Width="150"  ></asp:Label>
        </div>
        </HeaderTemplate>
        <ItemTemplate>
            <div>
                <asp:Label ID="Label3" runat="server"  Width="250"><%# Eval("perkCode") %></asp:Label>
                <asp:Label ID="Label5" runat="server"  Width="150"><%# Eval("upsellProductCode") %></asp:Label>
                <asp:Label ID="Label6" runat="server"  Width="150"><%# Eval("achievementCode") %></asp:Label>
            </div>
        </ItemTemplate>
    </asp:Repeater>

    <br />

    <div class="s2m_upsell_container">

        <div id="ABB1" runat="server" class="s2m_upsell">
            <div id="ABB1_head" runat="server" class="s2m_upsell_header">
                <h1><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB1_title_1") %></h1>
                <h2><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB1_title_2") %></h2>
               
            </div>
            <div id="ABB1_price_month" runat="server" class="s2m_upsell_price_month"><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB1_price_month") %></div>
            <div id="ABB1_price_year" runat="server" class="s2m_upsell_price_year"><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB1_price_year") %></div>
            <div id="ABB1_icon" runat="server" class="s2m_upsell_icon"></div>
            <div id="ABB1_desc" runat="server" class="s2m_upsell_desc"></div>
            <div id="ABB1_but" runat="server" class="s2m_upsell_but">Bestellen</div>
            <div class="s2m_upsell_surprise"></div>
        </div>

        <div id="ABB2" runat="server"  class="s2m_upsell">
            <div id="ABB2_head" runat="server" class="s2m_upsell_header">
                <h1><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB2_title_1") %></h1>
                <h2><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB2_title_2") %></h2>
            </div>
            <div id="ABB2_price_month" runat="server" class="s2m_upsell_price_month"><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB2_price_month") %></div>
            <div id="ABB2_price_year" runat="server" class="s2m_upsell_price_year"><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB2_price_year") %></div>
            <div id="ABB2_icon" runat="server" class="s2m_upsell_icon"></div>
            <div id="ABB2_desc" runat="server" class="s2m_upsell_desc"></div>
            <div id="ABB2_but" runat="server" class="s2m_upsell_but">Bestellen</div>
            <div class="s2m_upsell_surprise"></div>
        </div>

        <div id="ABB3" runat="server"  class="s2m_upsell">
            <div id="ABB3_head" runat="server" class="s2m_upsell_header">
                <h1><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB3_title_1") %></h1>
                <h2><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB3_title_2") %></h2>
            </div>
            <div id="ABB3_price_month" runat="server" class="s2m_upsell_price_month"><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB3_price_month") %></div>
            <div id="ABB3_price_year" runat="server" class="s2m_upsell_price_year"><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("ABB3_price_year") %></div>
            <div id="ABB3_icon" runat="server" class="s2m_upsell_icon"></div>
            <div id="ABB3_desc" runat="server" class="s2m_upsell_desc"></div>
            <div id="ABB3_but" runat="server" class="s2m_upsell_but">Bestellen</div>
            <div class="s2m_upsell_surprise"></div>
        </div>

        <div id="COMBI1" runat="server"  class="s2m_upsell">
            <div id="COMBI1_head" runat="server" class="s2m_upsell_header">
                <h1><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("COMBI1_title_1") %></h1>
                <h2><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("COMBI1_title_2") %></h2>
            </div>
            <div id="COMBI1_price_month" runat="server" class="s2m_upsell_price_month"><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("COMBI1_price_month") %></div>
            <div id="COMBI1_price_year" runat="server" class="s2m_upsell_price_year"><%= S2M.Resources.Upsell.Upsell.ResourceManager.GetString("COMBI1_price_year") %></div>
            <div id="COMBI1_icon" runat="server" class="s2m_upsell_icon"></div>
            <div id="COMBI1_desc" runat="server" class="s2m_upsell_desc"></div>
            <div id="COMBI1_but" runat="server" class="s2m_upsell_but">Bestellen</div>
            <div class="s2m_upsell_surprise"></div>
        </div>
    </div>

</asp:Content>

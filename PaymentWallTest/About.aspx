﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="PaymentWallTest.About" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
        <asp:Repeater ID="RepeaterSubscriptions" runat="server" >
              <HeaderTemplate>
                <div>
                    <asp:Label ID="Label1" runat="server" Text="logType" Font-Bold="True" Width="75" ></asp:Label>
                    <asp:Label ID="Label2" runat="server" Text="userId" Font-Bold="True" Width="75"  ></asp:Label>
                    <asp:Label ID="Label8" runat="server" Text="goodsId" Font-Bold="True" Width="75"  ></asp:Label>
                    <asp:Label ID="Label9" runat="server" Text="period" Font-Bold="True" Width="75"  ></asp:Label>
                    <asp:Label ID="Label10" runat="server" Text="periodLength" Font-Bold="True" Width="100"  ></asp:Label>
                    <asp:Label ID="Label16" runat="server" Text="status" Font-Bold="True" Width="75"  ></asp:Label>
                    <asp:Label ID="Label17" runat="server" Text="statusCode" Font-Bold="True" Width="150"  ></asp:Label>
                    <asp:Label ID="Label12" runat="server" Text="createdOn" Font-Bold="True" Width="150"  ></asp:Label>

                </div>
              </HeaderTemplate>
            <ItemTemplate>
                <div>
                    <asp:Label ID="Label3" runat="server"  Width="75"><%# Eval("logType") %></asp:Label>
                    <asp:Label ID="Label4" runat="server"  Width="75"><%# Eval("userId") %></asp:Label>
                    <asp:Label ID="Label5" runat="server"  Width="75"><%# Eval("goodsId") %></asp:Label>
                    <asp:Label ID="Label6" runat="server"  Width="75"><%# Eval("period") %></asp:Label>
                    <asp:Label ID="Label7" runat="server"  Width="100"><%# Eval("periodLength") %></asp:Label>
                    <asp:Label ID="Label13" runat="server"  Width="75"><%# Eval("status") %></asp:Label>
                    <asp:Label ID="Label14" runat="server"  Width="150"><%# Eval("statusCode") %></asp:Label>
                    <asp:Label ID="Label11" runat="server"  Width="150"><%# Eval("createdOn") %></asp:Label>

                    
                </div>
            </ItemTemplate>
        </asp:Repeater>
</asp:Content>



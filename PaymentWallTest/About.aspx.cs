﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain.PaymentWall;

namespace PaymentWallTest
{
    public partial class About : Page
    {
        private static List<s2m_log_paymentWall> logList;

        protected void Page_Load(object sender, EventArgs e)
        {
            BindActions();
        }

        public void BindActions()
        {

            DateTime endDate = DateTime.Now;
            DateTime startDate = endDate.AddMonths(-6);
            logList = s2m_log_paymentWall.GetList(startDate, endDate);
            RepeaterSubscriptions.DataSource = logList;
            RepeaterSubscriptions.DataBind();
        }
    }
}